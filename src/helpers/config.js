var path;
if (location.host == "localhost") {
  path = "http://localhost/adv-web-final-project/";
} else {
  path = "https://classmate.slimapps.tech/";
}
export default path;
