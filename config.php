<?php
define("DEV_BUILD", true);
// Send confirmation link (useless in DEV mode unless xampp is set to send fake emails)
define("SEND_LINK", false);

// Define DB Params
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "final_project");

// Define URL
define("ROOT_PATH", "/adv-web-final-project/");
define("ROOT_URL", "http://localhost/adv-web-final-project/");
